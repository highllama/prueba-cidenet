# CIDENET-API Documentacion

### Ambiente de Desarrollo

version de Node: [Node.js](https://nodejs.org/) v14.15.1

version de React: [React.js](https://reactjs.org/) v17.0.1

> Posteriormente de haber clonado el repositorio, proceder a instalar las dependencias necesarias añadir el archivo .env (el cual fue enviado en el correo) a la raiz de la carpeta backend

```sh
$ cd frontend
$ npm install || yarn install
```

> Ejecutar el servidor, el cual abrira automaticamente el navegador

```sh
$ npm start
```

> Ejecutar una nueva terminal

```sh
$ cd backend
$ npm install || yarn install
$ npm run dev
```

> luego de haber realizado los anteriores pasos, la aplicacion estara lista para su uso

# ENDPOINTS

## /api/user--- _GET_

retorna un Array con todos los usuarios registrados

| Propiedad       | Tipo de Dato |
| --------------- | ------------ |
| first_lastname  | String       |
| second_lastname | String       |
| name            | String       |
| other_name      | String       |
| email           | String       |
| user_count      | Number       |
| job_location    | String       |
| num_id          | String       |
| id_type         | String       |

## /api/user/check-email -- _POST_

recibe como parametros en el body de la peticion:  
|Propiedad | Tipo de Dato |  
|----------------|--------|
| first_lastname| String|  
| name| String|

## /api/user -- _POST_

recibe como parametros en el body de la peticion:  
|Propiedad | Tipo de Dato |  
|----------------|--------|
| first_lastname| String|  
|second_lastname| String |
|name| String |
|other_name| String|  
|email| String |
|job_location| String|  
|num_id| String |
|id_type| String|

## /api/user -- _PUT_

recibe como parametros en el body de la peticion:  
|Propiedad | Tipo de Dato |  
|----------------|--------|
| first_lastname| String|  
|second_lastname| String |
|name| String |
|other_name| String|  
|job_location| String|  
|num_id| String |
|id_type| String|  
|monogID| String|

## /api/user -- _DELETE_

recive como parametro en el body de la peticion un **mongoID**

## SCREENSHOTS

![Desktop laptop (1360px )](./frontend/src/assets/img/listar.png)

![Desktop laptop (1360px )](./frontend/src/assets/img/registrar.png)

### mobile (368px)

![mobile (368px )](./frontend/src/assets/img/mobile.png)
