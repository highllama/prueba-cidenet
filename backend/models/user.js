const mongoose = require("mongoose");

const schema = mongoose.Schema;

const userSchema = schema(
  {
    first_lastname: {
      type: String,
      required: true,
    },
    second_lastname: {
      type: String,
    },
    name: {
      type: String,
      required: true,
    },
    other_name: {
      type: String,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    job_location: {
      required: true,
      type: String,
    },
    num_id: {
      type: String,
      unique: true,
      required: true,
    },
    id_type: {
      required: true,
      type: String,
    },
    user_count: {
      type: Number,
      default: 1,
    },
    estado: {
      type: String,
      default: "Activo",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("User", userSchema);
