const User = require("../models/user");

async function getUsers(req, res) {
  try {
    const user = await User.find();
    res.status(200).send(user);
  } catch (error) {
    res.status(400).send({ error });
  }
}

async function checkEmail(req, res) {
  try {
    let { name, first_lastname } = req.body;
    console.log(name, first_lastname);
    const user = await User.findOne({ name, first_lastname });
    user.user_count += 1;
    user.save();
    console.log("increase");
    res.status(200).send({ user });
  } catch (error) {
    res.send({ user: { user_count: 0 } });
  }
}

async function createUSer(req, res) {
  try {
    const {
      first_lastname,
      second_lastname,
      name,
      other_name,
      email,
      job_location,
      num_id,
      id_type,
    } = req.body;
    const user = User({
      first_lastname,
      second_lastname,
      name,
      other_name,
      email,
      job_location,
      num_id,
      id_type,
    });
    const userSaved = await user.save();
    res.status(201).send({ status: "ok", userSaved });
  } catch (error) {
    res.status(400).send({ error });
  }
}

async function deleteUser(req, res) {
  try {
    const { mongoID } = req.body;
    console.log(mongoID);
    const user = await User.findByIdAndDelete(mongoID);
    res.status(200).send(user);
  } catch (error) {
    console.log(error);
  }
}

async function updateUser(req, res) {
  try {
    const {
      mongoID,
      first_lastname,
      second_lastname,
      name,
      other_name,
      job_location,
      num_id,
      id_type,
    } = req.body;
    const user = await User.updateOne(
      { _id: mongoID },
      {
        first_lastname,
        second_lastname,
        name,
        other_name,
        job_location,
        num_id,
        id_type,
      }
    );

    res.status(201).send(user);
  } catch (error) {
    console.log(error);
    res.status(400).send({ error });
  }
}

module.exports = {
  getUsers,
  createUSer,
  checkEmail,
  deleteUser,
  updateUser,
};
