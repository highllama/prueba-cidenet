const express = require("express");
const {
  getUsers,
  createUSer,
  checkEmail,
  deleteUser,
  updateUser,
} = require("../controllers/userController");

const user = (app) => {
  const router = express.Router();
  app.use("/api/user", router);

  router.post("/", createUSer);
  router.get("/", getUsers);
  router.post("/check-email", checkEmail);
  router.delete("/", deleteUser);
  router.put("/", updateUser);
};

module.exports = user;
