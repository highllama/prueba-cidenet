import React from "react";
//STYLE
import "../assets/style/components/Header.scss";
//RACT ROUTER
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className="Header">
      <div className="Header__main wrapper">
        <div>
          <Link to="/">
            <img
              src="https://cidenet.com.co/wp-content/themes/cidenet/images/cidenet-logo-100.png"
              alt="logo"
            />
          </Link>
        </div>
        <div>
          <nav>
            <ul>
              <Link to="/registrar-empleado">
                <li>Registrar Empleado</li>
              </Link>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
};

export default Header;
