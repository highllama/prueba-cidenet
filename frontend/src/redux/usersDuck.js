import Axios from "axios";
import Swal from "sweetalert2";

//CONSTANTES
const initialData = {
  data: [],
  user: {
    user_count: 0,
  },
};

const BASE_URL = "http://localhost:3001/api";

//TYPES
const GET_USERS = "GET_USERS";
const CHECK_MAIL = "CHECK_MAIL";
//REDUCER

export default function Moviesreducer(state = initialData, action) {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        data: action.payload,
      };
    case CHECK_MAIL:
      return {
        ...state,
        user: action.payload,
      };

    default:
      return state;
  }
}

//ACTIONS

export const registerUserAction = (form, mail) => async (dispatch) => {
  try {
    const {
      primer_apellido,
      segundo_apellido,
      primer_nombre,
      otros_nombres,
      pais_empleo,
      numero_id,
      tipo_id,
    } = form;
    const data = await Axios.post(`${BASE_URL}/user`, {
      email: mail,
      first_lastname: primer_apellido,
      second_lastname: segundo_apellido,
      name: primer_nombre,
      job_location: pais_empleo,
      num_id: numero_id,
      id_type: tipo_id,
      other_name: otros_nombres,
    });
  } catch (error) {
    Swal.fire({
      title: "Ocurrio un error",
      text: "Por favor intente de nuevo",
      icon: "warning",
      confirmButtonText: "Lo Hare!",
    });
    console.log(error);
  }
};

export const getUsersAction = () => (dispatch, getState) => {
  fetch(`${BASE_URL}/user`)
    .then((res) => res.json())
    .then((data) =>
      dispatch({
        type: GET_USERS,
        payload: data,
      })
    )
    .catch((err) => console.log(err));
};

export const deleteUserAction = (id) => async (dispatch) => {
  try {
    console.log(id);
    const data = await Axios.delete(`${BASE_URL}/user`, {
      data: { mongoID: id },
    });
    console.log(data);
  } catch (error) {
    console.log(error);
  }
};

export const updateUserAction = (id, form) => async () => {
  try {
    const data = await Axios.put(`${BASE_URL}/user`, {
      first_lastname: form.primer_apellido,
      second_lastname: form.segundo_apellido,
      name: form.primer_nombre,
      other_name: form.otros_nombres,
      job_location: form.pais_empleo,
      id_type: form.tipo_id,
      num_id: form.numero_id,
      mongoID: id,
    });
    console.log(data);
  } catch (error) {
    console.log(error);
  }
};

export const checkMailAction = (name, first_lastname) => async (
  dispatch,
  getState
) => {
  try {
    const data = await Axios.post(`${BASE_URL}/user/check-email`, {
      name,
      first_lastname,
    });
    if (data) {
      dispatch({
        type: CHECK_MAIL,
        payload: data.data.user,
      });
    }
  } catch (error) {
    console.log(error.message);
  }
};
