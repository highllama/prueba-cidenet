import "../assets/style/App.scss";
//RACT ROUTER
import { BrowserRouter, Route, Switch } from "react-router-dom";
//COMPONENTS
import Header from "../components/Header";
//CONTAINERS
import RegisterUser from "../containers/RegisterUser";
import ListUser from "../containers/ListUser";
//REDUX
import { Provider } from "react-redux";
import Store from "../redux/store";
function App() {
  return (
    <div className="App">
      <Provider store={Store()}>
        <BrowserRouter>
          <Header />
          <Switch>
            <Route path="/" component={ListUser} exact />
            <Route path="/registrar-empleado" component={RegisterUser} exact />
          </Switch>
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
