import React, { useState, useRef } from "react";
//STYLE
import "../assets/style/components/RegisterUser.scss";
//REDUX
import { checkMailAction, registerUserAction } from "../redux/usersDuck";
import { useDispatch, useSelector } from "react-redux";

const RegisterUser = () => {
  const initvalues = {
    primer_apellido: "",
    segundo_apellido: "",
    primer_nombre: "",
    otros_nombres: "",
    pais_empleo: "COLOMBIA",
    tipo_id: "CEDULA DE CIUDADANIA",
  };
  const dispatch = useDispatch();
  const store = useSelector((store) => store.usersDuck.user);
  const FORM = useRef();

  const [mail, setMail] = useState("");
  const [form, setValues] = useState(initvalues);

  // console.log(store);

  const Handleform = (e) => {
    setValues({
      ...form,
      [e.currentTarget.name]: e.currentTarget.value.toUpperCase(),
    });
  };

  const createMail = (name, first_lastname) => {
    dispatch(checkMailAction(name, first_lastname));
  };

  const validateForm = (form) => {
    let {
      primer_apellido,
      segundo_apellido,
      primer_nombre,
      otros_nombres,
      pais_empleo,
      numero_id,
    } = form;
    let status = false;
    let mayus = /^(([^ñÑ\d\s]){1,20})$/;
    let mayus50 = /^(([^ñÑ\d]){1,50})$/;
    let idTest = /^([a-zA-Z0-9-]+){2,20}$/;
    let dominio, mail;
    mayus.test(primer_apellido) &&
    mayus.test(segundo_apellido) &&
    mayus.test(primer_nombre) &&
    mayus50.test(otros_nombres) &&
    idTest.test(numero_id)
      ? (status = true)
      : (status = false);

    if (status) console.log("ok");

    pais_empleo === "ESTADOS UNIDOS"
      ? (dominio = "cidenet.com.us")
      : (dominio = "cidenet.com.co");

    createMail(primer_nombre, primer_apellido);

    let apellido = primer_apellido.replace(/\s+/g, "").toLowerCase();
    if (store.user_count > 1) {
      mail = `${primer_nombre.toLowerCase()}.${apellido}.${
        store.user_count + 1
      }@${dominio}`;
      setMail(mail);
    } else {
      mail = `${primer_nombre.toLowerCase()}.${apellido}@${dominio}`;
      setMail(mail);
    }
  };

  const handleSubmit = (form, data, mail) => {
    form.preventDefault();
    dispatch(registerUserAction(data, mail));
  };

  // console.log(form);

  return (
    <div className="RegisterUser">
      <div className="RegisterUser__main wrapper">
        <h2>Registrar Empleado</h2>
        <div>
          <form ref={FORM} onSubmit={(e) => handleSubmit(e, form, mail)}>
            <label>
              Primer Apellido
              <input
                name="primer_apellido"
                type="text"
                value={form.primer_apellido}
                onChange={Handleform}
                required={true}
              />
            </label>
            <label>
              Segundo Apellido
              <input
                type="text"
                name="segundo_apellido"
                value={form.segundo_apellido}
                onChange={Handleform}
                required={true}
              />
            </label>
            <label>
              Primer Nombre
              <input
                type="text"
                name="primer_nombre"
                value={form.primer_nombre}
                onChange={Handleform}
                required={true}
              />
            </label>
            <label>
              Otros Nombres
              <input
                type="text"
                name="otros_nombres"
                value={form.otros_nombres}
                onChange={Handleform}
              />
            </label>
            <label>
              Pais del Empleo
              <select
                name="pais_empleo"
                value={form.pais_empleo}
                onChange={Handleform}
              >
                <option value="COLOMBIA">Colombia</option>
                <option value="ESTADOS UNIDOS">Estados Unidos</option>
              </select>
            </label>
            <label>
              Tipo de Identificacion
              <select name="tipo_id" value={form.tipo_id} onChange={Handleform}>
                <option value="CEDULA DE CIUDADANIA">
                  Cedula de Ciudadania
                </option>
                <option value="CEDULA DE EXTRANJERIA">
                  Cédula de Extranjería
                </option>
                <option value="PASAPORTE">Pasaporte</option>
                <option value="PERMISO ESPECIAL">Permiso Especial</option>
              </select>
            </label>
            <label>
              Número de Identificación
              <input
                type="text"
                name="numero_id"
                value={form.numero_id}
                onChange={Handleform}
                required={true}
              />
            </label>
            <button onClick={() => validateForm(form)}>validar</button>
          </form>
          {mail !== "" && <h2>EMAIL: {mail}</h2>}
        </div>
      </div>
    </div>
  );
};

export default RegisterUser;
