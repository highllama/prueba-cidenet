import React from "react";
//STYLE
import "../assets/style/components/Modal.scss";
//REDUX
import { deleteUserAction, getUsersAction } from "../redux/usersDuck";
import { useDispatch } from "react-redux";
const Modal = ({ items, setModal }) => {
  const dispatch = useDispatch();

  const handleDelete = () => {
    items.map((item) => dispatch(deleteUserAction(item._id)));
    setModal(false);
  };

  return (
    <div className="Modal">
      <div className="Modal__container">
        <div>
          <h2>¿Esta seguro que desea eliminar?</h2>
          <div className="Modal__main">
            <button onClick={() => handleDelete()}>SI</button>
            <button onClick={() => setModal(false)}>NO</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
