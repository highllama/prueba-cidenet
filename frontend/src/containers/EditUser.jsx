import React, { useEffect, useState, useRef } from "react";
//STYLE
import "../assets/style/components/EditUser.scss";
//REDUX
import { useSelector, useDispatch } from "react-redux";
import { updateUserAction } from "../redux/usersDuck";
//
import Swal from "sweetalert2";

const EditUser = ({ id, setModal }) => {
  const dispatch = useDispatch();
  const store = useSelector((store) => store.usersDuck);
  const [form, setValues] = useState();
  const FORM = useRef();
  console.log(form, id);

  useEffect(() => {
    if (store.data) {
      const user = store.data.filter((item) => item._id === id);
      //   console.log(store);
      if (user[0]) {
        const {
          first_lastname,
          second_lastname,
          name,
          other_name,
          job_location,
          id_type,
          num_id,
        } = user[0];
        setValues({
          primer_apellido: first_lastname,
          segundo_apellido: second_lastname,
          primer_nombre: name,
          otros_nombres: other_name,
          pais_empleo: job_location,
          tipo_id: id_type,
          numero_id: num_id,
        });
      }
    }
  }, [store]);

  const Handleform = (e) => {
    setValues({
      ...form,
      [e.currentTarget.name]: e.currentTarget.value.toUpperCase(),
    });
  };
  const handleSubmit = (e, form) => {
    e.preventDefault();
    dispatch(updateUserAction(id, form));
    Swal.fire({
      title: "Actualizado!",
      text: "se ha actualizado correctamente el usuario",
      icon: "success",
    }).then((result) => {
      setModal(false);
    });
  };

  return (
    <div className="EditUser">
      <div className="EditUser__container">
        <div className="EditUser__main">
          {form && (
            <form ref={FORM} onSubmit={(e) => handleSubmit(e, form)}>
              <label>
                Primer Apellido
                <input
                  name="primer_apellido"
                  type="text"
                  value={form.primer_apellido}
                  onChange={Handleform}
                  required={true}
                />
              </label>
              <label>
                Segundo Apellido
                <input
                  type="text"
                  name="segundo_apellido"
                  value={form.segundo_apellido}
                  onChange={Handleform}
                  required={true}
                />
              </label>
              <label>
                Primer Nombre
                <input
                  type="text"
                  name="primer_nombre"
                  value={form.primer_nombre}
                  onChange={Handleform}
                  required={true}
                />
              </label>
              <label>
                Otros Nombres
                <input
                  type="text"
                  name="otros_nombres"
                  value={form.otros_nombres}
                  onChange={Handleform}
                />
              </label>
              <label>
                Pais del Empleo
                <select
                  name="pais_empleo"
                  value={form.pais_empleo}
                  onChange={Handleform}
                >
                  <option value="COLOMBIA">Colombia</option>
                  <option value="ESTADOS UNIDOS">Estados Unidos</option>
                </select>
              </label>
              <label>
                Tipo de Identificacion
                <select
                  name="tipo_id"
                  value={form.tipo_id}
                  onChange={Handleform}
                >
                  <option value="CEDULA DE CIUDADANIA">
                    Cedula de Ciudadania
                  </option>
                  <option value="CEDULA DE EXTRANJERIA">
                    Cédula de Extranjería
                  </option>
                  <option value="PASAPORTE">Pasaporte</option>
                  <option value="PERMISO ESPECIAL">Permiso Especial</option>
                </select>
              </label>
              <label>
                Número de Identificación
                <input
                  type="text"
                  name="numero_id"
                  value={form.numero_id}
                  onChange={Handleform}
                  required={true}
                />
              </label>
              <br />
              <button>ACTUALIZAR</button>
              <button onClick={() => setModal(false)}>CANCELAR</button>
            </form>
          )}
        </div>
      </div>
    </div>
  );
};

export default EditUser;
