import React, { useEffect, useState } from "react";
//REDUX
import { useDispatch, useSelector } from "react-redux";
import { getUsersAction } from "../redux/usersDuck";
//COMPONENTS
import DataTable from "react-data-table-component";
import Modal from "./Modal";
//STYLE
import "../assets/style/components/ListUser.scss";
import EditUser from "./EditUser";

const ListUser = () => {
  const [state, setState] = useState({});
  const [deleteModal, setDeleteModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const store = useSelector((store) => store.usersDuck);
  const dispatch = useDispatch();
  const data = store.data;
  const columns = [
    {
      name: "Primer Nombre",
      selector: "name",
      sortable: true,
      grow: 0.25,
    },
    {
      name: "Primer Apellido",
      selector: "first_lastname",
      sortable: true,
      grow: 0.6,
    },
    {
      name: "Segundo Apellido",
      selector: "second_lastname",
      sortable: true,
      grow: 0.6,
    },
    {
      name: "Otros Nombres",
      selector: "other_name",
      sortable: true,
      grow: 0.6,
    },
    {
      name: "ID",
      selector: "num_id",
      sortable: true,
      grow: 0.6,
    },
    {
      name: "Tipo ID",
      selector: "id_type",
      sortable: true,
      grow: 0.6,
    },
    {
      name: "Locacion",
      selector: "job_location",
      sortable: true,
      grow: 0.5,
    },
    {
      name: "Correo",
      selector: "email",
      sortable: true,
      allowOverflow: true,
    },
  ];

  const handleChange = (e) => {
    setState(e);
  };

  useEffect(() => {
    dispatch(getUsersAction());
  }, [deleteModal, editModal]);
  return (
    <div className="ListUser">
      <div className="wrapper">
        <div>
          <DataTable
            onSelectedRowsChange={handleChange}
            selectableRowsHighlight
            pagination={true}
            selectableRows
            title="Empleados"
            columns={columns}
            data={data}
          ></DataTable>
        </div>
        <div className="ListUser__buttons">
          {state.selectedCount >= 1 && (
            <button onClick={() => setDeleteModal(true)}>
              ELIMINAR SELECCIONADOS
            </button>
          )}
          {state.selectedCount < 2 && state.selectedCount > 0 && (
            <button onClick={() => setEditModal(true)}>EDITAR</button>
          )}
          {deleteModal && (
            <Modal items={state.selectedRows} setModal={setDeleteModal} />
          )}
          {editModal && (
            <EditUser id={state.selectedRows[0]._id} setModal={setEditModal} />
          )}
        </div>
      </div>
    </div>
  );
};

export default ListUser;
